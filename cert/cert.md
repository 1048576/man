# cert

## 1. Часто используемые команды

### 1.1. Просмотреть справку

```sh
openssl [genrsa|x509|req|pkcs12] -help
```

### 1.2. Просмотреть сертификат

```sh
openssl x509 -noout -text -in ./ca.crt -certopt no_pubkey,no_sigdump -alias -purpose
```

### 1.3. Просмотреть запрос

```sh
openssl req -noout -text -in ./client.csr -reqopt no_pubkey,no_sigdump
```

### 1.4. Просмотреть pkcs12

```sh
openssl pkcs12 -in ./store.pfx -password pass:1048576
```

## 2. Создание хранилища сертификатов

### 2.1. Создаем конфигурационные файлы и скрипты

```sh
echo 01 > ca.srl

cat <<'EOF' > openssl.cnf
[ req ]
encrypt_key = no
default_bits = 4096
default_md = sha256
distinguished_name = req_distinguished_name

[ req_distinguished_name ]
commonName = hostname

[ ca_extensions ]
basicConstraints = CA:true
keyUsage = keyCertSign, cRLSign
subjectKeyIdentifier = hash

[ client_extensions ]
basicConstraints = CA:false
keyUsage = digitalSignature, keyEncipherment
extendedKeyUsage = clientAuth
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always
EOF

cat <<'EOF' > create-crt
#!/bin/bash

set -o errexit
set -o pipefail

PURPOSE=${1}
DIR=${2}
CN=${3}

mkdir -p ${DIR}

openssl req \
    -config ./openssl.cnf \
    -new \
    -keyout ${DIR}/${CN}.key \
    -out ${DIR}/${CN}.csr \
    -subj "/CN=${CN}"

openssl x509 -req \
    -extfile ./openssl.cnf \
    -extensions ${PURPOSE}_extensions \
    -CA ./ca.crt \
    -CAkey ./ca.key \
    -CAserial ./ca.srl \
    -CAcreateserial \
    -text \
    -certopt no_pubkey,no_sigdump \
    -alias \
    -purpose \
    -in ${DIR}/${CN}.csr \
    -out ${DIR}/${CN}.crt

rm ${DIR}/${CN}.csr
EOF

chmod u+x create-crt
```

#### Cоздание корневого сертификата

```sh
# Создаем ключ
openssl genrsa -out ./ca.key 4096
# Создаем самоподписанный корневой сертификат
openssl req \
  -config ./openssl.cnf \
  -extensions ca_extensions \
  -new \
  -x509 \
  -key ./ca.key \
  -out ./ca.crt \
  -subj "/CN=yourdomain.com"
```

#### Создание сертификата-клиента

```sh
./create-crt client ./certs host.yourdomain.com
```

#### Проверка handshake через curl

```sh
sudo curl -v --cacert ./ca.crt --cert ./client.crt --key ./client.key https://localhost:4560
```

#### Скрипт создания сертификата

```sh
#!/bin/bash

name="$1"
serial="$2"
password="1048576"
caname="ca"
cacrt="./ca.crt"
cakey="./ca.key"
subj="/C=/ST=/L=/O=/OU=/CN=${name}"

target="/tmp/cert/${name}"
key="${target}/${name}.key"
csr="${target}/${name}.csr"
crt="${target}/${name}.crt"
pfx="${target}/${name}.pfx"
keystore="${target}/${name}.p12"
truststore="${target}/truststore.p12"

mkdir -p "${target}" &&
openssl genrsa -out ${key} 4096 &&
openssl req -new -key ${key} -out ${csr} -subj="${subj}" &&
openssl x509 -req -days 3285 -CA ${cacrt} -CAkey ${cakey} -set_serial ${serial} -in ${csr} -out ${crt} &&
rm ${csr} &&
openssl pkcs12 -export -out ${pfx} -in ${crt} -inkey ${key} -certfile ${cacrt} -password "pass:${password}" -name ${name} -caname ${caname} &&
cp ${cacrt} ${target}/${caname}.crt &&

rm -f ${keystore} &&
${JAVA_HOME}/bin/keytool -importkeystore \
    -srckeystore ${pfx} -srcstorepass ${password} -srcstoretype pkcs12 \
    -destkeystore ${keystore} -deststorepass ${password} -deststoretype pkcs12 &&

rm -f ${truststore} &&
${JAVA_HOME}/bin/keytool -importcert -noprompt -keystore ${truststore} -storepass ${password} -storetype pkcs12  -alias ${caname} -file ${cacrt}
```

#### Скрипт проверки

```sh
#!/bin/bash

host=
password=
keystore=
truststore=

java -Djavax.net.debug=none -jar /opt/ssltest/ssltest.jar \
    -hiderejects \
    -keystore ${keystore} \
    -keystorepassword ${password} \
    -verify-hostname \
    -truststore ${truststore} \
    -truststorepassword ${password} \
    -check-certificate \
    ${host}

```
