# Logstash: 4. Создание сертификатов

Для создания сертификатов используем скрипт. **Внимание!** Нужно установить **java**

## 1. Создаем каталог **/opt/logstash-cert** и файл скрипта **/opt/logstash-cert/gen-crt.sh**

```sh
sudo mkdir -p /opt/logstash-cert
sudo touch /opt/logstash-cert/gen-crt.sh
sudo chmod o= /opt/logstash-cert/gen-crt.sh
sudo chmod u+x /opt/logstash-cert/gen-crt.sh
ls -la /opt/logstash-cert
```

## 2. Добавляем содержимое в **/opt/logstash-cert/gen-crt.sh**

```sh
#!/bin/bash

JAVA_HOME="/usr/"

name="$1"
serial="$2"
password="1048576"
caname="ca.elk"
cacrt="/etc/logstash-cert/ca.elk.crt"
cakey="/etc/logstash-cert/ca.elk.key"
subj="/C=UA/ST=Kyiv/L=Kyiv/O=Nexstep Solutions LLC/OU=IT/CN=${name}"

target="/tmp/cert/${name}"
key="${target}/${name}.key"
csr="${target}/${name}.csr"
crt="${target}/${name}.crt"
pfx="${target}/${name}.pfx"
keystore="${target}/${name}.p12"
truststore="${target}/trustore.p12"

mkdir -p "${target}" &&
openssl genrsa -out ${key} 4096 &&
openssl req -new -key ${key} -out ${csr} -subj="${subj}" &&
openssl x509 -req -days 3285 -CA ${cacrt} -CAkey ${cakey} -set_serial ${serial} -in ${csr} -out ${crt} &&
rm ${csr} &&
openssl pkcs12 -export -out ${pfx} -in ${crt} -inkey ${key} -certfile ${cacrt} -password "pass:${password}" -name ${name} -caname ${caname} &&
cp ${cacrt} ${target}/${caname}.crt &&

rm -f ${keystore} &&
${JAVA_HOME}/bin/keytool -importkeystore \
    -srckeystore ${pfx} -srcstorepass ${password} -srcstoretype pkcs12 \
    -destkeystore ${keystore} -deststorepass ${password} -deststoretype pkcs12 &&

rm -f ${truststore} &&
${JAVA_HOME}/bin/keytool -importcert -noprompt -keystore ${truststore} -storepass ${password} -storetype pkcs12  -alias ${caname} -file ${cacrt}
```

## 3. Пример использования

Создадим сертификаты для сервера **db-master**

```sh
sudo /opt/logstash-cert/gen-crt.sh db-master 05
```
