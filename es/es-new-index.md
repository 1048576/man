# Elasticsearch: 3. Создание нового индекса

При добавлении данных в индекс, которого нет в **elasticsearch**, база данных создаст его автоматически. При этом настройки этого индекса он скопирует из шаблонов

## 1. Для получения списка шаблонов используем команду

```sh
curl http://localhost:9200/_template?pretty
```

## 2. Создание/изменение нового шаблона для индекса

**ВНИМАНИЕ!** В примере создается шаблон для индексов **nginx-access**

2.1. Придумываем новое имя шаблона (**nginx-access**). Имя передается через **url-path** (**/_template/nginx-access**)

2.2. Определяем список регулярных выражений для имен индексов, которые должны использовать этот шаблон. У нас используется только один шаблон __nginx-access-*__. Этот список указывается в блоке данных **.index_patterns**

```json
{
    "index_patterns" : [
        "nginx-access-*"
    ]
}
```

2.3. Задаем настройки индекса в блоке данных **.settings**

```json
"settings" : {
    "index" : {
        "codec" : "best_compression",
        "refresh_interval" : "30s",
        "number_of_shards" : "1",
        "translog" : {
            "retention" : {
                "size" : "0"
            },
            "sync_interval" : "60s",
            "durability" : "async"
        },
        "number_of_replicas" : "0"
    }
}
```

2.4. Объявляем поля и их типы данных в блоке-данных **.mappings** (**ВНИМАНИЕ!** Объявляем только поля, для которых хотим указать тип данных). Если поле не объявлено или если тип данных не указан, то **elasticsearch** поле все равно добавит и сам определит его тип данных.

**ВНИМАНИЕ!** Следует обратить особое внимание на блок **.mappings.doc**. Конкретно на **doc**. Это мета-поле **_type** и оно помечено как **deprecated** (ранее это поле могло принимать разные значения, сейчас только одно)

```sh
curl -XPUT http://localhost:9200/_template/nginx-access \
    -H "Content-Type: application/json" \
    -d '{
        "order" : 0,
        "index_patterns" : [
            "nginx-access-*"
        ],
        "settings" : {
            "index" : {
                "codec" : "best_compression",
                "refresh_interval" : "30s",
                "number_of_shards" : "1",
                "translog" : {
                    "retention" : {
                        "size" : "0"
                    },
                    "sync_interval" : "60s",
                    "durability" : "async"
                },
                "number_of_replicas" : "0"
            }
        },
        "mappings" : {
            "doc" : {
                "properties" : {
                    "api_version" : {
                        "type" : "short"
                    },
                    "http_x_forwarded_for" : {
                        "type" : "ip"
                    },
                    "remote_addr" : {
                        "type" : "ip"
                    },
                    "total_time" : {
                        "type" : "float"
                    },
                    "res" : {
                        "properties" : {
                            "status" : {
                                "type" : "short"
                            },
                            "body_bytes_sent" : {
                                "type" : "long"
                            }
                        }
                    }
                }
            }
        }
    }'
```
