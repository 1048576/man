# Elasticsearch: 2. Открытие индекса elasticsearch на запись после нехватки места на диске

В случае достижения заполненности диска на уровне 90% (это настройка high water mark), elasticsearch закрывает индексы на запись для избежания потери данных. Признак, который за это отвечает **.settings.index.blocks.read_only_allow_delete**

#### Получение настроек индекса

```sh
# curl <адрес-elasticsearch>/<имя-индекса>/_settings?pretty
curl http://localhost:9200/app--2018.08.03/_settings?pretty
```

Правильно настроенный индекс должен иметь такие настройки (на момент написания статьи)

```json
"settings": {
    "index":{
        "codec": "best_compression",
        "refresh_interval":"30s",
        "number_of_shards":"1",
        "translog": {
            "retention": {
                "size":"0"
            },
            "sync_interval":"60s",
            "durability":"async"
        },
        "blocks": {
            "read_only_allow_delete":"false"
        },
        "number_of_replicas":"0",
    }
}
```

#### Изменение настроек индекса(ов)

Elasticsearch позволяет менять настройку сразу нескольких индексов одним запросом. Для этого можго использовать метасимволы "__*__", "__?__" или оператор "__,__". Например:
* для разблокировки всех индексов используем запрос

```sh
curl -XPUT localhost:9200/*/_settings?pretty \
    -H "Content-Type: application/json" \
    -d '{
        "index": {
            "blocks": {
                "read_only_allow_delete" : "false"
            }
        }
    }'
```

* для внесения правок во все индексы __app--2018.07.*__(за июль) и индекс __app--2018.08.03__

```sh
curl -XPUT localhost:9200/app--2018.07.*,app--2018.08.03/_settings?pretty \
    -H "Content-Type: application/json" \
    -d '{
        "index": {
            "blocks": {
                "read_only_allow_delete" : "false"
            }
        }
    }'
```
