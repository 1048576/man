# PostgreSQL: 1.6. Настройка балансирощика pgbouncer

### Общий моменты в настройке пула для

* Количество подключений к пулу (упрощенно) ограничено только оперативной памятью, поэтому допустимо разрешать 300 подключений (1 подключение ~ 3 мб RAM)
* Максимальное количество запросов (которое postgresql может обрабатывать одновременно) примерно равно количеству ядер на хостовой машине. Нужно стремиться к тому, чтобы пул подключений к postgresql не превосходил этого количества. Однако, поскольку бывают моменты, когда приложение открывает транзакцию и после этого уходит в режим ожидания (чего быть не должно в OLTP системах), то увеличение пула подключений в 2 раза является допустимым

## 1. Установка pgbouncer

```sh
sudo apt update
sudo apt install -y pgbouncer
```

## 2. Настраиваем pgbouncer (*/etc/pgbouncer/pgbouncer.ini*)

### 2.1. Прописываем пути подлючения к БД

```ini
[databases]
; - <имя бд> = <параметры подключения>
; - auth_user - пользователь, под которым идет проверка наличия пользователя в БД

;   Используется, чтобы не настраивать список пользователей
; - user - пользователь, под которым будут выполняться запросы

replica = host=db-replica auth_user=pgbouncer
```

### 2.2. Привязываем pgbouncer к порту и адресу

```ini
listen_addr = *
listen_port = 6432
```

### 2.3. [опционально] Настраиваем ssl между postgresql и pgbouncer

Необходимы следующие сертификаты и ключи:
* **ca-db.crt** - корневой сертификат
* **pgbouncer.key** и **pgbouncer.crt** - сертификат для пользователя **pgbouncer**

2.3.1. Создаем каталог **/etc/pgbouncer/cert/**

```sh
sudo mkdir /etc/pgbouncer/cert/
sudo chown -R postgres:postgres /etc/pgbouncer/cert
sudo chmod -R og= /etc/pgbouncer/cert
```

2.3.2. Добавляем сертификаты и ключи

2.3.3. Раздача прав

```sh
sudo chown -R postgres:postgres /etc/pgbouncer
sudo chmod -R og= /etc/pgbouncer
sudo ls -la /etc/pgbouncer/cert
```

2.3.4. Добавляем парамерты в настройки pgbouncer (*TLS settings for connecting to backend databases*)

```ini
server_tls_sslmode = verify-full
server_tls_ca_file = /etc/pgbouncer/cert/ca-db.crt
server_tls_key_file = /etc/pgbouncer/cert/pgbouncer.key
server_tls_cert_file = /etc/pgbouncer/cert/pgbouncer.crt
```

2.3.5. Проверяем, что с хоста, где находится балансировщик, можно поключиться к бд

```sh
sudo psql "port=5432 host=db-replica user=pgbouncer sslcert=/etc/pgbouncer/cert/pgbouncer.crt sslkey=/etc/pgbouncer/cert/pgbouncer.key sslrootcert=/etc/pgbouncer/cert/ca-db.crt sslmode=verify-full"
```

### 2.4. Настраиваем ssl между pgbouncer и приложением

Необходимы следующие сертификаты и ключи:
* **ca-db.crt** - корневой сертификат
* **db-replica.key** и **db-replica.crt** - сертификат для pgbouncer (использую сертификат от postgresql)

2.4.1. Добавляем парамерты в настройки pgbouncer (*TLS settings for accepting clients*)

```ini
client_tls_sslmode = verify-full
client_tls_ca_file = /etc/pgbouncer/cert/ca-db.crt
client_tls_key_file = /etc/pgbouncer/cert/db-replica.key
client_tls_cert_file = /etc/pgbouncer/cert/db-replica.crt
```

2.4.2. Добавляем парамерты в настройки pgbouncer (*Authentication settings*)

```ini
auth_type = hba
auth_hba_file = /etc/pgbouncer/pg_bouncer_hba.conf
auth_query = SELECT usename, passwd FROM pg_shadow WHERE usename=$1
```

**ВНИМАНИЕ!** Желательно убрать параметр *auth_file* из конфигурационного файла и удалить файл **/etc/pgbouncer/userlist.txt**

2.4.3. Создаем и настраиваем *pg_bouncer_hba.conf*

2.4.3.1. Создаем */etc/pgbouncer/pg_bouncer_hba.conf*

```sh
sudo touch /etc/pgbouncer/pg_bouncer_hba.conf
sudo chown -R postgres:postgres /etc/pgbouncer
sudo chmod -R og= /etc/pgbouncer
sudo ls -la /etc/pgbouncer/pg_bouncer_hba.conf
```

2.4.3.2. Добавляем в *pg_bouncer_hba.conf* содержимое

```sh
hostssl all             all             127.0.0.1/32            cert
host    all             all             127.0.0.1/32            md5
hostssl all             all             192.168.0.0/24          cert
host    all             all             192.168.0.0/24          md5
```

### 2.5. Настраиваем правило воврата подключения в пул

В пул возвращаем подключение после закрытия транзакции

```ini
pool_mode = transaction
```

### 2.6. Включаем возврат ресурсов ОС после возвращения подключения в пул

```ini
server_reset_query = DISCARD ALL
```

### 2.7. Включаем игнорирование параметра *extra_float_digits*

Балансировщик не поддерживает этот параметр, а драйвер jdbc будет пытаться его установить и падать

```ini
ignore_startup_parameters = extra_float_digits
```

### 2.8. Настраиваем размер пула

* *max_client_conn* - максимальное количество подключений к балансировщику (1 подкючение ~ 3 мб RAM)
* *default_pool_size* - количество реальных подключений к postgresql, которые балансировщик будет всегда держать открытыми. Устанавливаем равным количеству ядер на хосте CУБД
* *reserve_pool_size* - количество дополнительных подключений к postgresql (если основной пул исчерпан). Устанавливаем равным количеству ядер на хосте CУБД
* *reserve_pool_timeout* - время ожидания освобождения подключения к postgresql до использования резервного пула

```ini
max_client_conn = 200
default_pool_size = 10
reserve_pool_size = 10
reserve_pool_timeout = 2
```

### 2.9. Проверка настройки балансировщика

```sh
sudo psql "port=6432 host=db-replica user=pgbouncer sslcert=/etc/pgbouncer/cert/pgbouncer.crt sslkey=/etc/pgbouncer/cert/pgbouncer.key sslrootcert=/etc/pgbouncer/cert/ca-db.crt sslmode=verify-full"
```
