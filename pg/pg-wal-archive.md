# PostgreSQL: 1.5. Настройка архивации wal-файлов

## 1. Установка пакетов

* устанавливаем архиватор zstd в **/opt/zstd/bin/zstd**
* устанавливаем gpg версии 2
* устанавливаем pv

## 2. Создаем ssh ключи для доступа к хранилищу и ключ шифрования wal-файлов

2.1. Сохраняем ключ шифрования wal-файлов в **/etc/postgresql/remote-store/passphrase**

```sh
sudo mkdir /etc/postgresql/remote-store/
sudo touch /etc/postgresql/remote-store/passphrase
sudo chown -R postgres:postgres /etc/postgresql/remote-store
sudo chmod -R og= /etc/postgresql/remote-store
```

2.2. Добавляем ssh ключи для подключения к хранилищу в **/etc/postgresql/remote-store**.

2.3. Раздача прав

```sh
sudo chown -R postgres:postgres /etc/postgresql/remote-store
sudo chmod -R og= /etc/postgresql/remote-store
sudo ls -la /etc/postgresql/remote-store
```

## 3. Создаем скрипт архивации

3.1. Создаем файл **/var/lib/postgresql/scripts/archive-wal.sh**

```sh
sudo mkdir /var/lib/postgresql/scripts
sudo touch /var/lib/postgresql/scripts/archive-wal.sh
sudo chown -R postgres:postgres /var/lib/postgresql/scripts
sudo chmod -R 700 /var/lib/postgresql/scripts/archive-wal.sh
sudo ls -la /var/lib/postgresql/scripts
```

3.2. Добавляем в него содержимое

**ВНИМАНИЕ!!!** Нужно проверить, что:
* правильно указан **walStoreHost**
* правильно указан **walStoreDir**
* правильно указан **passphraseFile**
* правильно указан **sshKeyFile**

```sh
#!/bin/bash

walStoreDir=/mnt/db/wal
walStoreHost=rootdb@filestore
walPath=$1
walName=$2

sshKeyFile=/etc/postgresql/remote-store/id_rsa
passphraseFile=/etc/postgresql/remote-store/passphrase

# Сжатие
/opt/zstd/bin/zstd ${walPath} -10 -c -T8 -q | pv -cN zstd -TbW |
# Шифрование
gpg --batch --symmetric --passphrase-file ${passphraseFile} --cipher-algo AES256 | pv -cN gpg -TbW |
# Запись в хранилище
ssh -i ${sshKeyFile} -o StrictHostKeyChecking=no ${walStoreHost} "cat > ${walStoreDir}/${walName}.zstd.gpg"
```
Для debian ниже 9 в скрипте необходимо использовать ```gpg2```

3.3. Проверяем, что все правильно настроено

```sh
touch /var/tmp/test.wal
sudo /var/lib/postgresql/scripts/archive-wal.sh /var/tmp/test.wal test.wal
rm /var/tmp/test.wal
```

## 4. Настраиваем postgresql

4.1. Создаем конфигурационный файл **conf.d/archive.conf**

```sh
# Для postgresql 10 и кластера main
sudo touch /etc/postgresql/10/main/conf.d/archive.conf
sudo chown postgres:postgres /etc/postgresql/10/main/conf.d/archive.conf
sudo chmod og= /etc/postgresql/10/main/conf.d/archive.conf
sudo ls -la /etc/postgresql/10/main/conf.d/archive.conf
```

4.2. Добавляем в него содержимое

```sh
archive_mode = on
archive_command = '/var/lib/postgresql/scripts/archive-wal.sh %p %f'
```
