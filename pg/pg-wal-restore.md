# PostgreSQL: 1.9. Применение wal-файлов к базе данных, восстановленной из бэкапа

**ВНИМАНИЕ!** В примерах используется кластер **10/main**

## 1. Установка пакетов

* устанавливаем архиватор zstd в **/opt/zstd/bin/zstd**
* устанавливаем gpg версии 2

## 2. Создаем ssh ключи для доступа к хранилищу и ключ шифрования wal-файлов

2.1. Сохраняем ключ шифрования wal-файлов в **/etc/postgresql/remote-store/passphrase**

```sh
sudo mkdir /etc/postgresql/remote-store/
sudo touch /etc/postgresql/remote-store/passphrase
sudo chown -R postgres:postgres /etc/postgresql/remote-store
sudo chmod -R og= /etc/postgresql/remote-store
```

2.2. Добавляем ssh ключи для подключения к хранилищу в **/etc/postgresql/remote-store**

2.3. Раздача прав

```sh
sudo chown -R postgres:postgres /etc/postgresql/remote-store
sudo chmod -R og= /etc/postgresql/remote-store
sudo ls -la /etc/postgresql/remote-store
```

## 3. Восстанавливаем базу данных из бэкапа.

После восстановления из бэкапа постгрес запускать нельзя.

## 4. Настраиваем кластер для работы с удаленным хранилищем wal-файлов

4.1. Создаем файл **/var/lib/postgresql/scripts/restore-wal.sh**

```shell
sudo mkdir /var/lib/postgresql/scripts
sudo touch /var/lib/postgresql/scripts/restore-wal.sh
sudo chown -R postgres:postgres /var/lib/postgresql/scripts
sudo chmod -R 700 /var/lib/postgresql/scripts/restore-wal.sh
sudo ls -la /var/lib/postgresql/scripts
```

4.2. Добавляем в него содержимое

**ВНИМАНИЕ!!!** Нужно проверить, что:
* правильно указан **walStoreDir**
* правильно указан **walStoreHost**
* правильно указан **passphraseFile**
* правильно указан **sshKeyFile**
* правильно указан **pgData**

```sh
#!/bin/bash

walStoreDir=/mnt/db/wal
walStoreHost=rootdb@filestore
# Ключ шифрования
passphraseFile=/etc/postgresql/remote-store/passphrase
# Ключ для доступа к хранилищу по ssh
sshKeyFile=/etc/postgresql/remote-store/id_rsa
pgData=/var/lib/postgresql/10/main
recoveryConf=${pgData}/recovery.conf

restore_command="'ssh -i ${sshKeyFile} -o StrictHostKeyChecking=no ${walStoreHost} \"cat ${walStoreDir}/%f.zstd.gpg\" | gpg2 --batch --passphrase-file ${passphraseFile} --cipher-algo AES256 --decrypt --quiet | /opt/zstd/bin/zstd -q -d -o %p'"

sudo su postgres -c "touch ${recoveryConf}" &&
sudo su postgres -c "chmod og= ${recoveryConf}" &&
echo "restore_command = ${restore_command}" | sudo tee ${recoveryConf} &&
echo -e "# Этот параметр указывает именованную точку восстановления (созданную с помощью pg_create_restore_point()), до которой будет производиться восстановление.\n#recovery_target_name = 'daily backup 2011-01-26'" | sudo tee -a ${recoveryConf} &&
echo -e "# Данный параметр указывает точку времени, вплоть до которой будет производиться восстановление.\n recovery_target_time = '2018-07-14 22:39:00 EEST' " | sudo tee -a ${recoveryConf}
```
Пример конфигурационного файла для восстановления до 2018-07-20 16:39:00:
```
restore_command = 'ssh -i /var/lib/postgresql/10/main/recovery-sshKeyFile -o StrictHostKeyChecking=no rootdb@filestore "cat /mnt/db/wal/%f.zstd.gpg" | gpg --batch --passphrase-file /var/lib/postgresql/10/main/recovery-passphraseFile --cipher-algo AES256 --decrypt --quiet | /opt/zstd/zstd -q -d -o %p'
# Этот параметр указывает именованную точку восстановления (созданную с помощью pg_create_restore_point()), до которой будет производиться восстановление.
#recovery_target_name = 'daily backup 2011-01-26'
# Данный параметр указывает точку времени, вплоть до которой будет производиться восстановление.
recovery_target_time = '2018-07-20 16:39:00 EEST'
```
Если оба параметра recovery_target_name и recovery_target_time будут закомментированными, то восстановление пройдет до последнего найденного wal-файла

4.3. Выполняем скрипт

```sh
/var/lib/postgresql/scripts/restore-wal.sh
```

## 5. Стартуем кластер

```sh
sudo pg_ctlcluster 10/main start
```

**ВНИМАНИЕ!** Скорее всего кластер сразу не запустится из-за несогласованности параметров. Открываем лог postgresql и смотрим в каком параметре проблема. Меняем этот параметр в конфигурационных файлах и пробуем запуститься вновь

```sh
sudo tail -f /var/log/postgresql/postgresql-10-main.log
```

## 6. Проверяем в логах, что восстановление началось

```sh
sudo tail -f /var/log/postgresql/postgresql-10-main.log
```

В логах должны быть записи примерно следующего вида

```sh
2018-08-27 12:20:19.397 EEST [1820] LOG:  restored log file "00000001000001090000003E" from archive
2018-08-27 12:20:20.920 EEST [1820] LOG:  restored log file "00000001000001090000003F" from archive
2018-08-27 12:20:22.292 EEST [1820] LOG:  restored log file "000000010000010900000040" from archive
2018-08-27 12:20:23.613 EEST [1820] LOG:  restored log file "000000010000010900000041" from archive
2018-08-27 12:20:25.020 EEST [1820] LOG:  restored log file "000000010000010900000042" from archive
```
