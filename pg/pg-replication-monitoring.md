# PostgreSQL: 2.4. Мониторинг репликации

Нужно выполнить запрос на базе **db-master**

```sql
select slot_name, pg_wal_lsn_diff(pg_current_wal_lsn(), restart_lsn) / 1048576 mb
from pg_catalog.pg_replication_slots
where slot_name in ('replica', 'reports')
```

Запрос вернет отставание в мб wal-файлов базы **db-master** от базы **db-replica** и **db-reports**
