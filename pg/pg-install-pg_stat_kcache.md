# PostgreSQL: 1.10. Установка расширения pg_stat_kcache для мониторинга

**ВНИМАНИЕ!** Установка и настройка описаны в https://github.com/powa-team/pg_stat_kcache и могу поменяться. На момент написание это делалось так

## 1. Установка расширения

```sh
sudo apt update
sudo apt install -y git make gcc
git clone https://github.com/powa-team/pg_stat_kcache.git
cd pg_stat_kcache
sudo apt install postgresql-server-dev-10
make
sudo make checkinstall
```

Данная установка выполнит:
```
/bin/mkdir -p '/usr/lib/postgresql/10/lib'
/bin/mkdir -p '/usr/share/postgresql/10/extension'
/bin/mkdir -p '/usr/share/postgresql/10/extension'
/usr/bin/install -c -m 755  pg_stat_kcache.so '/usr/lib/postgresql/10/lib/pg_stat_kcache.so'
/usr/bin/install -c -m 644 .//pg_stat_kcache.control '/usr/share/postgresql/10/extension/'
/usr/bin/install -c -m 644 .//pg_stat_kcache--2.1.1.sql .//pg_stat_kcache--2.1.0.sql .//pg_stat_kcache--2.1.0--2.1.1.sql '/usr/share/postgresql/10/extension/'
```

## 2. Настраиваем postgresql

2.1. Создаем конфигурационный файл **conf.d/pgstat.conf**

```sh
# Для postgresql 10 и кластера main
sudo touch /etc/postgresql/10/main/conf.d/pgstat.conf
sudo chown postgres:postgres /etc/postgresql/10/main/conf.d/pgstat.conf
sudo chmod og= /etc/postgresql/10/main/conf.d/pgstat.conf
sudo ls -la /etc/postgresql/10/main/conf.d/pgstat.conf
```

2.2. Добавляем в него содержимое

```sh
shared_preload_libraries = 'pg_stat_statements,pg_stat_kcache'

pg_stat_statements.max = 500
pg_stat_statements.track = all
pg_stat_statements.track_utility = true
pg_stat_statements.save = false

track_activity_query_size=16384
```

## 3. Создаем расширение pg_stat_kcache для базы production

```sh
sudo -u postgres psql --cluster=10/main -d production -c "CREATE EXTENSION pg_stat_kcache;"
```
