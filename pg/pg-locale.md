# PostgreSQL: 1.4. Настройка локализации

1.1. Создаем конфигурационный файл **conf.d/locale.conf**

```sh
# Для postgresql 10
sudo touch /etc/postgresql/10/main/conf.d/locale.conf
sudo chown postgres:postgres /etc/postgresql/10/main/conf.d/locale.conf
sudo chmod og= /etc/postgresql/10/main/conf.d/locale.conf
sudo ls -la /etc/postgresql/10/main/conf.d/locale.conf
```

1.2. Добавляем в него содержимое

```sh
lc_messages = 'ru_UA.UTF-8'
lc_monetary = 'ru_UA.UTF-8'
lc_numeric = 'ru_UA.UTF-8'
lc_time = 'ru_UA.UTF-8'
timezone = 'localtime'
log_timezone = 'localtime'
datestyle = 'iso, dmy'
```
