# PostgreSQL: 2.1. Репликация. Настройка Настройка master базы

**ВНИМАНИЕ**. Все примеры написаны для кластера **10/main**

## 1. **(для логической репликации, db-reports)** Создаем пользователя-репликатора (**поменять пароль**)

```sh
# Пароль нужно поменять
sudo -u postgres psql --cluster=10/main -c "create role replicator login replication password '111'"
```

## 2. Добавляем в конфигурационный файл **pg_hba.conf** разрещение на подключения с **db-reports** и **db-replica**

```sh
hostssl all             all             192.168.0.0/24          cert
# Для репликации с db-replica
hostssl replication     postgres        192.168.0.0/24          cert
```

## 3. Изменяем в конфигурации кластера параметры **wal_level** и **max_replication_slots**

3.1. Создаем файл **conf.d/replication.conf**

```sh
# Для postgresql 10
sudo touch /etc/postgresql/10/main/conf.d/replication.conf
sudo chown postgres:postgres /etc/postgresql/10/main/conf.d/replication.conf
sudo chmod og= /etc/postgresql/10/main/conf.d/replication.conf
sudo ls -la /etc/postgresql/10/main/conf.d/replication.conf
```

3.2. Добавляем в него содержимое

```sh
# Для потоковой репликации достаточно wal_level = replica
# Для потоковой и логической нужно wal_level = logical
wal_level = logical

# max_replication_slots = (количество логических реплик + количество потоковых реплик + 1)
max_replication_slots = 3
```

## 4. **(для потоковой репликации, db-replica)** Создаем физический слот репликации

Это нужно для того, чтобы в случае недоступности реплики, postgresql не удалил wal-файлы, которые реплике будут нужны после восстановления ее работы

```sh
# pg_create_physical_replication_slot(<название слота репликации>);"
sudo -u postgres psql --cluster=10/main -c "select pg_create_physical_replication_slot('replica');"
```
