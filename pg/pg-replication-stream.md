# PostgreSQL: 2.2. Потоковая репликация. Настройка db-replica

**ВНИМАНИЕ**. Все примеры написаны для кластера **10/main**

## 1. Устанавливаем и настраиваем postgresql нужной версии

## 2. Создаем каталог для сертификатов и подкладываем в него сертификаты для пользователя **postgres**

2.1. Создаем каталог

```sh
sudo mkdir /etc/postgresql/cert
sudo chown postgres:postgres /etc/postgresql/cert
sudo chmod og= /etc/postgresql/cert
sudo ls -la /etc/postgresql/cert
```

2.2. Кладем в каталог **ca-db.crt**, **postgres.crt**, **postgres.key** (как создавать написано [здесь](pg-ssl.md))

2.3. Раздаем права

```sh
sudo chown -R postgres:postgres /etc/postgresql/cert
sudo chmod -R og= /etc/postgresql/cert
```

## 3. Удаляем каталог с данными кластера

```sh
# rm -rf /var/lib/postgresql/<версия postgresql>/<имя кластера>
sudo rm -rf /var/lib/postgresql/10/main
```

## 4. Запускаем первичную инициализацию

```sh
sudo -u postgres pg_basebackup \
    --dbname="host=db-master user=postgres sslcert=/etc/postgresql/cert/postgres.crt sslkey=/etc/postgresql/cert/postgres.key sslrootcert=/etc/postgresql/cert/ca-db.crt sslmode=verify-full" \
    --pgdata=/var/lib/postgresql/10/main \
    --slot=replica \
    --wal-method=stream --write-recovery-conf
```

## 5. Стартуем кластер

**ВНИМАНИЕ**!!! Скорее всего, сразу не поднимется из-за несовместимости конфигурационных файлов. В этом случае открываем лог файл и смотрим, на какой параметр ругается. Копируем его значение с боевого сервера и пробуем снова

```sh
# pg_ctlcluster <версия postgresql> <имя кластера> <команда>
sudo pg_ctlcluster 10 main start
```

## 6. Дополнительные опции репликации
Есть возможность задать отставание реплики от боевой базы и т.д. Эти параметры задаются через конфигурационный файл

```sh
# /var/lib/postgresql/<версия postgresql>/<имя кластера>/recovery.conf
mcedit /var/lib/postgresql/10/main/recovery.conf
```

## 7. Превращаем реплику в боевую базу

Нужно удалить конфигурационный файл recovery.conf и перегрузить кластер

```sh
# /var/lib/postgresql/<версия postgresql>/<имя кластера>/recovery.conf
rm /var/lib/postgresql/10/main/recovery.conf
# pg_ctlcluster <версия postgresql> <имя кластера> <команда>
sudo pg_ctlcluster 10 main restart
```
