# PostgreSQL: 1.7. Настройка бэкапа базы данных

## 1. Установка пакетов

* устанавливаем архиватор zstd в **/opt/zstd/bin/zstd**
* устанавливаем gpg версии 2
* устанавливаем pv
* устанавливаем curl

## 2. Создаем ssh ключи для доступа к хранилищу и ключ шифрования архива

2.1. Сохраняем ключ к архиву в **/etc/postgresql/remote-store/passphrase**

```sh
sudo mkdir /etc/postgresql/remote-store/
sudo touch /etc/postgresql/remote-store/passphrase
sudo chown -R postgres:postgres /etc/postgresql/remote-store
sudo chmod -R og= /etc/postgresql/remote-store
```

2.2. Добавляем ssh ключи для подключения к хранилищу в **/etc/postgresql/remote-store**

2.3. Раздача прав

```sh
sudo chown -R postgres:postgres /etc/postgresql/remote-store
sudo chmod -R og= /etc/postgresql/remote-store
sudo ls -la /etc/postgresql/remote-store
```

## 3. Создаем скрипт создания бэкапа

3.1. Создаем файл **/var/lib/postgresql/scripts/make-backup.sh**

```sh
sudo mkdir /var/lib/postgresql/scripts
sudo touch /var/lib/postgresql/scripts/make-backup.sh
sudo chown -R postgres:postgres /var/lib/postgresql/scripts
sudo chmod -R 700 /var/lib/postgresql/scripts/make-backup.sh
sudo ls -la /var/lib/postgresql/scripts
```

3.2. Добавляем в него содержимое

**ВНИМАНИЕ!!!** Нужно проверить, что:
* правильно указан **backupStoreDir**
* правильно указан **backupStoreHost**
* правильно указан **passphraseFile**
* правильно указан **sshKeyFile**
* правильно указан **ciUser** - пользователь jenkins, у которого есть права на запуск сборки по созданию клона **USER[:PASSWORD]**. Вместо пароля используем api-токен
* правильно указан **errorFlagFile** - файл, которые создается в случае падения процедуры бэкапа

```sh
#!/bin/bash

backupStoreDir=/mnt/db/backup
backupStoreHost=rootdb@filestore
backupName=$(date +%Y-%m-%dT%H:%M).tar.zstd.gpg
passphraseFile=/etc/postgresql/remote-store/passphrase
sshKeyFile=/etc/postgresql/remote-store/id_rsa
ciUser=
errorFlagFile=/var/tmp/db-make-backup-error.flag

cd /var/tmp &&
# Создание
sudo -u postgres pg_basebackup --cluster=10/main --pgdata=- --format=tar --wal-method=fetch | pv -cN backup -Tb |
# Сжатие
/opt/zstd/bin/zstd - -10 -c -T8 | pv -cN zstd -TbW |
# Шифрование
gpg2 --batch --symmetric --passphrase-file ${passphraseFile} --cipher-algo AES256 | pv -cN gpg -TbW |
# Запись в хранилище
ssh -i ${sshKeyFile} -o StrictHostKeyChecking=no ${backupStoreHost} "cat > ${backupStoreDir}/${backupName}" &&
# Сохраняем имя последнего успешного backup-файла
ssh -i ${sshKeyFile} -o StrictHostKeyChecking=no ${backupStoreHost} "echo '${backupName}' > ${backupStoreDir}/last" &&
rm -f ${errorFlagFile} || touch ${errorFlagFile}
```

Для debian ниже 9 в скрипте необходимо использовать ```gpg2```

3.3. Проверяем, что все правильно настроено

```sh
sudo /var/lib/postgresql/scripts/make-backup.sh
```

## 4. Создаем задачу в cron по ежедневному созданию бэкапа

4.1. Открываем список задач

```sh
sudo crontab -e
```

4.2. Добавляем новую задачу

```sh
10 4 * * * /var/lib/postgresql/scripts/make-backup.sh
```
