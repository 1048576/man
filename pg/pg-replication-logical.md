# PostgreSQL: 2.3. Логическая репликация. Настройка db-reports

## 1. На боевой базе (*db-master*)

1.1. Создаем публикацию в базе данных (публикация - это список таблиц, которые мы хотим реплицировать). Публикаций может быть несколько (на момент написания была только reports)

1.1.1. Запрос, который нам сгенерирует psql команду на создание **новой** публикации

```sql
select 'sudo -u postgres psql -d master -c ''create publication reports for table ' || string_agg(schemaname || '."' || tablename || '"', ',') || ''''
    from pg_tables
    where schemaname='public'
```

Он примерно выглядит вот так

```sh
sudo -u postgres psql -d master -c 'create publication reports for table public."motivation_receiving_template",public."user",public."company",public."policy_kz_driver",public."policy",...'
```

1.1.2. Запрос, который нам сгенерирует psql команду на добавление новых таблиц в **существующую** публикации

```sql
select 'sudo -u postgres psql -d master -c ''alter publication reports add table ' || string_agg(t.schemaname || '."' || t.tablename || '"', ',') || ''''
    from pg_catalog.pg_tables t
        left join pg_catalog.pg_publication_tables pt
            on (pt.schemaname = t.schemaname) and (pt.tablename = t.tablename) and (pt.pubname = 'reports')
    where (t.schemaname='public') and (pt.tablename is null)
```

Он примерно выглядит вот так

```sh
sudo -u postgres psql -d master -c 'alter publication reports add table public."policy",public."contract"'
```
## 2. На логической реплике (*db-reports*)

**ВНИМАНИЕ**. В примерах используется кластер логической репликации **10/main** и база **reports**

2.1. Создаем каталог для сертификатов и подкладываем в него сертификаты для пользователя **postgres**

2.1.1. Создаем каталог

```sh
sudo mkdir /etc/postgresql/cert
sudo chown postgres:postgres /etc/postgresql/cert
sudo chmod og= /etc/postgresql/cert
```

2.1.2. Кладем в каталог **ca-db.crt**, **replicator.crt**, **replicator.key** (как создавать написано [здесь](pg-ssl.md))

2.1.3. Раздаем права

```sh
sudo chown -R postgres:postgres /etc/postgresql/cert
sudo chmod -R og= /etc/postgresql/cert
sudo ls -la /etc/postgresql/cert
```

2.2. Создаем таблицы, которые будем реплицировать.
**ВНИМАНИЕ.** Для postgresql важно только наличие схем и таблиц. Для таблицы важно, что она содержала все столбцы, которые есть на боевой базе, но столбцов может быть и больше

Для dump мета данных можно выполнить скрипт на базе (**db-master**) и скопировать из него **create type**, **create table**, **alter table add constraint primary key**

```sh
sudo -u postgres pg_dump -d master --no-privileges --schema public --schema-only
```

2.3. Создаем подписку **reposts** на публикацию **reports** c боевой базы

```sql
sudo -u postgres psql --cluster=10/main --dbname=reports -c "create subscription reports connection 'host=db-master dbname=master user=replicator sslcert=/etc/postgresql/cert/replicator.crt sslkey=/etc/postgresql/cert/replicator.key sslrootcert=/etc/postgresql/cert/ca-db.crt sslmode=verify-full' publication reports"
```

2.4. Отслеживание первичной инициализации таблиц

При создании подписки, начинается процесс первичной синхронизации. Для того, чтобы отследить, какие таблицы его еще не прошли, можно выполнить запрос на **db-reports**

```sql
-- State code: i = initialize, d = data is being copied, s = synchronized, r = ready (normal replication)
select c.relname, srsubstate
    from pg_catalog.pg_subscription_rel r
        inner join pg_class c
            on c.oid = r.srrelid
    where srsubstate != 'r'
```
