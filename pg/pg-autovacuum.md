# PostgreSQL: 1.3. Настройка autovacuum

Общий подход такой: настраиваем агресивный вакуум со стороны бд и ограничиваем его со стороны операционной системы

## 1. Настраиваем postgresql

1.1. Создаем конфигурационный файл **conf.d/autovacuum.conf**

```sh
# Для postgresql 10
sudo touch /etc/postgresql/10/main/conf.d/autovacuum.conf
sudo chown postgres:postgres /etc/postgresql/10/main/conf.d/autovacuum.conf
sudo chmod og= /etc/postgresql/10/main/conf.d/autovacuum.conf
sudo ls -la /etc/postgresql/10/main/conf.d/autovacuum.conf
```

1.2. Добавляем в него содержимое

```sh
autovacuum = on
autovacuum_analyze_scale_factor = 0.05
autovacuum_max_workers = 10
autovacuum_vacuum_scale_factor = 0.01
autovacuum_vacuum_cost_delay = 10
```

## 2. Настраиваем операционную систему (cron)

2.1. Открываем список задач cron-а

```sh
sudo crontab -e
```

2.2. Добавляем в него содержимое

```sh
# Ограничиваем автовакуум по процессору
* * * * * /usr/bin/pgrep -f 'postgres: autovacuum worker' | xargs --no-run-if-empty -I $ renice -n 20 -p $ >/dev/null 2>/dev/null
# Ограничиваем автовакуум по диску
* * * * * /usr/bin/pgrep -f 'postgres: autovacuum worker' | xargs --no-run-if-empty -I $ ionice -c 3 -t -p $
```
