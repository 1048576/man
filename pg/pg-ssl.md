# PostgreSQL: 1.2. Настройка ssl

## 1. Создаем сертификаты

```sh
sudo mkdir /etc/postgresql/cert
sudo chown postgres:postgres /etc/postgresql/cert
sudo chmod og= /etc/postgresql/cert

# Создаем корневой сертификат ca-db
sudo openssl genrsa -out /etc/postgresql/cert/ca-db.key 4096
sudo openssl req -new -x509 -days 3650 -key /etc/postgresql/cert/ca-db.key -out /etc/postgresql/cert/ca-db.crt -subj "/CN=ca-db"

# Создаем сертификат сервера server-db
sudo openssl genrsa -out /etc/postgresql/cert/server-db.key 4096
sudo openssl req -new -key /etc/postgresql/cert/server-db.key -out /etc/postgresql/cert/server-db.csr -subj "/CN=server-db"
sudo openssl x509 -req -days 3285 -CA /etc/postgresql/cert/ca-db.crt -CAkey /etc/postgresql/cert/ca-db.key -set_serial 01 -in /etc/postgresql/cert/server-db.csr -out /etc/postgresql/cert/server-db.crt

# Создаем сертификат для пользователя postgres (для того, чтобы проверить правильность настройки). CN - должен содержать имя пользователя
sudo openssl genrsa -out /etc/postgresql/cert/postgres.key 4096
sudo openssl req -new -key /etc/postgresql/cert/postgres.key -out /etc/postgresql/cert/postgres.csr -subj "/CN=postgres"
sudo openssl x509 -req -days 3285 -CA /etc/postgresql/cert/ca-db.crt -CAkey /etc/postgresql/cert/ca-db.key -set_serial 02 -in /etc/postgresql/cert/postgres.csr -out /etc/postgresql/cert/postgres.crt

# Чистим за собой
sudo su -c "rm -f /etc/postgresql/cert/*.csr"
sudo chown -R postgres:postgres /etc/postgresql/cert
sudo chmod -R og= /etc/postgresql/cert
sudo su -c "ls -la /etc/postgresql/cert/*"
```

## 2. Настраиваем postgresql

2.1. Создаем конфигурационный файл **conf.d/cert.conf**

```sh
# Для postgresql 10
sudo touch /etc/postgresql/10/main/conf.d/cert.conf
sudo chown postgres:postgres /etc/postgresql/10/main/conf.d/cert.conf
sudo chmod og= /etc/postgresql/10/main/conf.d/cert.conf
sudo ls -la /etc/postgresql/10/main/conf.d/cert.conf
```

2.2. Добавляем в него содержимое

```sh
ssl_ca_file = '/etc/postgresql/cert/ca-db.crt'
ssl_cert_file = '/etc/postgresql/cert/server-db.crt'
ssl_key_file = '/etc/postgresql/cert/server-db.key'
```

## 3. Проверяем, что все сделали правильно

3.1. Добавляем в **pg_hba.conf** запись. Порядок имеет значение. Нужно подставить правильную маску адресов

```sh
# Подключение через сертификат
hostssl all             all             192.168.0.0/24          cert
```

3.2. Выполняем команду.

**ВНИМАНИЕ!** Имя хоста (host=) должно совпадать с именем в сертификате сервера, либо снизить безопасность подключения с *verify-full* на *require*

```sh
sudo psql "host=server-db user=postgres sslcert=/etc/postgresql/cert/postgres.crt sslkey=/etc/postgresql/cert/postgres.key sslrootcert=/etc/postgresql/cert/ca-db.crt sslmode=verify-full"
```
