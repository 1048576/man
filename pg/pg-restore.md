# PostgreSQL: 1.8. Восстановление базы данных из бэкапа

**ВНИМАНИЕ!** В примерах используется кластер **10/main**

## 1. Установка пакетов

* устанавливаем архиватор zstd в **/opt/zstd/bin/zstd**
* устанавливаем gpg версии 2

## 2. Создаем ssh ключи для доступа к хранилищу и ключ шифрования архива

2.1. Сохраняем ключ к архиву в **/etc/postgresql/remote-store/passphrase**

```sh
sudo mkdir /etc/postgresql/remote-store/
sudo touch /etc/postgresql/remote-store/passphrase
sudo chown -R postgres:postgres /etc/postgresql/remote-store
sudo chmod -R og= /etc/postgresql/remote-store
```

2.2. Добавляем ssh ключи для подключения к хранилищу в **/etc/postgresql/remote-store**

2.3. Раздача прав

```sh
sudo chown -R postgres:postgres /etc/postgresql/remote-store
sudo chmod -R og= /etc/postgresql/remote-store
sudo ls -la /etc/postgresql/remote-store
```

## 3. Создаем скрипт восстановления базы из бэкапа

3.1. Создаем файл **/var/lib/postgresql/scripts/restore-backup.sh**

```sh
sudo mkdir /var/lib/postgresql/scripts
sudo touch /var/lib/postgresql/scripts/restore-backup.sh
sudo chown -R postgres:postgres /var/lib/postgresql/scripts
sudo chmod -R 700 /var/lib/postgresql/scripts/restore-backup.sh
sudo ls -la /var/lib/postgresql/scripts
```

3.2. Добавляем в него содержимое

**ВНИМАНИЕ!!!** Нужно проверить, что:
* правильно указан **backupStoreDir**
* правильно указан **backupStoreHost**
* правильно указан **backupName**
* правильно указан **passphraseFile**
* правильно указан **sshKeyFile**
* правильно указан **pgData**

```sh
#!/bin/bash

backupStoreDir=/mnt/db/backup
backupStoreHost=rootdb@filestore
backupName="2018-07-17T04:10.tar.zstd.gpg"
# Ключ шифрования
passphraseFile=/etc/postgresql/remote-store/passphrase
# Ключ для доступа к хранилищу по ssh
sshKeyFile=/etc/postgresql/remote-store/id_rsa
pgData=/var/lib/postgresql/10/main

if [ -d ${pgData} ]; then
    echo "Нужно удалить каталог ${pgData}"
    exit 1
fi

sudo su postgres -c "mkdir ${pgData} --mode=700" &&
ssh -i ${sshKeyFile} -o StrictHostKeyChecking=no ${backupStoreHost} "cat ${backupStoreDir}/${backupName}" |
gpg2 --batch --passphrase-file ${passphraseFile} --cipher-algo AES256 --decrypt |
/opt/zstd/bin/zstd -d |
sudo su postgres -c "tar -x -C ${pgData}"
```

3.3. Выполняем скрипт

```sh
/var/lib/postgresql/scripts/restore-backup.sh
```
