# PostgreSQL
* 1. Установка новой базы
  * [1.1. Установка postgresql](pg/pg-install.md)
  * [1.2. Настройка ssl](pg/pg-ssl.md)
  * [1.3. Настройка autovacuum](pg/pg-autovacuum.md)
  * [1.4. Настройка локализации](pg/pg-locale.md)
  * [1.5. Настройка архивации wal-файлов](pg/pg-wal-archive.md)
  * [1.6. Настройка балансирощика pgbouncer](pg/pg-bouncer.md)
  * [1.7. Настройка бэкапа базы данных](pg/pg-backup.md)
  * [1.8. Восстановление базы данных из бэкапа](pg/pg-restore.md)
  * [1.9. Применение wal-файлов к базе данных, восстановленной из бэкапа](pg/pg-wal-restore.md)
  * [1.10. Установка расширения pg_stat_kcache для мониторинга](pg/pg-install-pg_stat_kcache.md)
  * [1.11. Установка приложения для мониторинга БД](https://gitlab.com/1048576/ts/pgmonitoring)
* 2. Репликация
  * [2.1. Репликация. Настройка master базы](pg/pg-replication-master.md)
  * [2.2. Потоковая репликация. Настройка db-replica](pg/pg-replication-stream.md)
  * [2.3. Логическая репликация. Настройка db-reports](pg/pg-replication-logical.md)
  * [2.4. Мониторинг репликации](pg/pg-replication-monitoring.md)

# Logstash-Elasticsearch
  * [1. Архивация и восстановления индекса elasticseach с помощью пакета esutils](https://gitlab.com/1048576/ts/esutils)
  * [2. Открытие индекса elasticsearch на запись после нехватки места на диске](es/es-unlock-index.md)
  * [3. Создание нового индекса](es/es-new-index.md)
  * [4. Создание сертификатов](es/es-cert.md)

# Certificate
  * [1. Создание и проверка сертификатов](cert/cert.md)
